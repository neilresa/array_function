<?php
	$food = array('fruits' => array('orange', 'banana', 'apple'),
				  'veggie' => array('carrot', 'collard', 'pea'));

	// recursive count
	echo count($food, COUNT_RECURSIVE); // output 8
	echo "<br>";

	// normal count
	echo count($food); // output 2
	echo "<br>";
	

	$a[0] = 1;
	$a[1] = 3;
	$a[2] = 5;
	$result = count($a);
	echo $result;

?>
